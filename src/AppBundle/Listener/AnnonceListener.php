<?php

namespace AppBundle\Listener;

use AppBundle\Entity\Annonce;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AnnonceListener
{
    private $tokenStorage ;
    public function __construct($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function preUpdate(Annonce $annonce, LifecycleEventArgs $event)
    {
        $annonce->setDateUpdate(new \DateTime());
        $user = $this->getUser();
        $annonce->setEditBy($user);
    }

    public function prePersist(Annonce $annonce, LifecycleEventArgs $event)
    {
        $annonce->setDateCreated(new \DateTime());
        $user = $this->getUser();
        $annonce->setEditBy($user);
    }

    protected function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
