<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Categorie
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategorieRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Categorie
{
    /**
     * @var $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column()
     */
    private $id;
    /**
     * @var $value
     *
     * @ORM\Column(type="string")
     */
    private $value;
    /**
     * @var $annonces
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Annonce", mappedBy="categorie", cascade={"remove"})
     */
    private $annonces;

    /**
     * @var $dateCreated
     *
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @var $dateUpdate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->dateCreated = new \DateTime();
    }
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->dateCreated = new \DateTime();
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->annonces = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Categorie
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Add annonce
     *
     * @param \AppBundle\Entity\Annonce $annonce
     *
     * @return Categorie
     */
    public function addAnnonce(\AppBundle\Entity\Annonce $annonce)
    {
        $this->annonces[] = $annonce;

        return $this;
    }

    /**
     * Remove annonce
     *
     * @param \AppBundle\Entity\Annonce $annonce
     */
    public function removeAnnonce(\AppBundle\Entity\Annonce $annonce)
    {
        $this->annonces->removeElement($annonce);
    }

    /**
     * Get annonces
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnnonces()
    {
        return $this->annonces;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Categorie
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Categorie
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }
}
