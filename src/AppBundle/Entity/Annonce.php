<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Article
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnnonceRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\EntityListeners({"AppBundle\Listener\AnnonceListener"})
 */
class Annonce
{
    /**
     * @var $id
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @var $content
     *
     * @ORM\Column(type="text")
     */
    private $content;
    /**
     * @var $categorie
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Categorie", inversedBy="annonces")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $categorie;

    /**
     * @var $dateCreated
     *
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @var $dateUpdate
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpdate;

    /**
     * @var $editBy
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    private $editBy;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Annonce
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set categorie
     *
     * @param \AppBundle\Entity\Categorie $categorie
     *
     * @return Annonce
     */
    public function setCategorie(\AppBundle\Entity\Categorie $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \AppBundle\Entity\Categorie
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

 
    

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Annonce
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return Annonce
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * Set editBy
     *
     * @param \AppBundle\Entity\User $editBy
     *
     * @return Annonce
     */
    public function setEditBy(\AppBundle\Entity\User $editBy = null)
    {
        $this->editBy = $editBy;

        return $this;
    }

    /**
     * Get editBy
     *
     * @return \AppBundle\Entity\User
     */
    public function getEditBy()
    {
        return $this->editBy;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->dateCreated = new \DateTime();
    }
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->dateCreated = new \DateTime();
    }
}
