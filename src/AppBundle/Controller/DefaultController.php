<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @param Request $request
     *
     * @Route("/", name="homepage")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render(
            'AppBundle:default:index.html.twig',
            [
                'message' => "hello world",
            ]
        );
    }

    /**
     * @param $parameter1
     * @param $parameter2
     *
     * @Route(
     *     "test_route/{parameter1}/{parameter2}",
     *     name="testRoute",
     * )
     *
     * @return mixed
     */
    public function testRouteAction($parameter1, $parameter2)
    {
        return $this->render(
            'AppBundle:default:testRoute.html.twig',
            [
                'title'      => 'test Route',
                'parameter1' => $parameter1,
                'parameter2' => $parameter2,
            ]
        );
    }

    /**
     * @param $parameter1
     * @param $parameter2
     *
     * @Route(
     *     "/articles",
     *     name="articles",
     * )
     *
     * @return mixed
     */
    public function articleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cathegories = $em->getRepository('AppBundle:Categorie')->findAll();
        return $this->render(
            'AppBundle:default:articles.html.twig',
            [
                'title'      => 'listes des articles',
                'categories' => $cathegories,
            ]
        );
    }
}
